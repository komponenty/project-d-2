package pk.glk.settingscontroller.internal;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

import java.util.concurrent.atomic.AtomicReference;
import java.lang.String;

import pk.glk.contracts.IChangeSetting;

@Component
public final class SettingsController
{
	private BundleContext context;
	
	@Reference
	private IChangeSetting ChangeSettings;	
	
	@Activate
	public void activate(BundleContext bc)
	{
		this.context = bc;
		System.out.println("SpellChecker bundle activated");
	}
	
	//connect component with IChangeSetting interface
	protected void bindChangeSettings(IChangeSetting setting)
	{
		this.ChangeSettings = setting;
	}

}

