package pk.glk.soundplayer.internal;


import java.io.File;
import java.io.IOException;
import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;
import java.lang.String;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import pk.glk.soundplayer.ISound;


@Component
@Service
public final class SoundPlayer
    implements ISound
{
	private BundleContext context;
        private Clip clip;
        AudioInputStream audioInputStream;
        
    
	@Activate
	public void activate(BundleContext bc)
	{
		this.context = bc;
		System.out.println("SoundPlayer bundle activated");
	}
	
	@Override
	public void PlaySound(String fileName, boolean multithreaded)
	{
            if(multithreaded)
            {
                final String path = fileName;
                new Thread(){public void run(){
                    try 
                    {
                         AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(path).getAbsoluteFile());
                         Clip clip = AudioSystem.getClip();
                         clip.open(audioInputStream);
                         clip.start();
                    } 

                    catch(IOException | LineUnavailableException | UnsupportedAudioFileException ex) 
                    {
                         System.out.println("Error with playing sound.");
                    }

               }}.start();
            }
            else
            {
                try 
                {
                     audioInputStream = AudioSystem.getAudioInputStream(new File(fileName).getAbsoluteFile());
                     clip = AudioSystem.getClip();
                     clip.open(audioInputStream);
                     clip.start();
                } 

                catch(IOException | LineUnavailableException | UnsupportedAudioFileException ex) 
                {
                     System.out.println("Error with playing sound.");
                }
            }
	}  

        @Override
        public void StopPlaying() {
            clip.stop();
        }
        
        
}

