package pk.glk.bbcode.internal;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

import java.lang.String;

import pk.glk.contracts.IBBCode;

@Component
@Service
public final class BBCoder
    implements IBBCode
{
	private BundleContext context;
	
	@Activate
	public void activate(BundleContext bc)
	{
		this.context = bc;
		System.out.println("BBCode bundle activated");
	}
	
	@Override
	public String BBCodeToHtml(String BBCodeString)
	{
		return "";
	}
	
	@Override
	public String HtmlToBBCode(String HtmlString)
	{
		return "";
	}
    
}

