package pk.glk.logger.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pk.glk.logger.ILogger;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

import java.lang.String;

import pakman.logger.contract.Logger;

@Component
@Service
public final class GLKLogger	
    implements ILogger
{
	private BundleContext context;
        
        @Reference 
        private Logger PakmanLogger;
        
        protected void bindPakmanLogger(Logger pl)
        {
            this.PakmanLogger = pl;
            System.out.println("---- BINDING PAKMAN LOGGER: "+ PakmanLogger);
        }
    
	@Activate
	public void activate(BundleContext bc)
	{
		this.context = bc;
		System.out.println("Logger bundle activated");
	}

        @Override
        public void WriteLine(String log) {
            PakmanLogger.WriteLine(log);
        }

        @Override
        public void WriteLine(String log, String invoker) {
            PakmanLogger.WriteLine(log, invoker);
        }

        @Override
        public void writeLine(Exception e) {
            PakmanLogger.writeLine(e);
        }
}

