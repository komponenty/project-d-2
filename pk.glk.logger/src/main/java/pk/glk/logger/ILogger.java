package pk.glk.logger;

/**
 * Public API representing an example OSGi service
 */
public interface ILogger
{
        void WriteLine(String log);

        void WriteLine(String log, String invoker);

        void writeLine(Exception e);
}

