package pk.glk.contracts;



public interface IConfigInfo
{  
    int GetNumber();
    String GetPassword();
    boolean GetRememberMe();
}




