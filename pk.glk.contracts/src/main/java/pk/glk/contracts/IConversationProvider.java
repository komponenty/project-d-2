package pk.glk.contracts;

import java.util.List;
import pk.glk.contracts.IMessage;
import pk.glk.contracts.IContactInfo;

public interface IConversationProvider
{  
        boolean SendMessage(IMessage message);
	IMessage ReciveMessage(IContactInfo contact);
	List<IMessage> ReciveAllMessages();
	String GetSecretKey();
        IMessage CreateNewMessage(int sender, String message, int receiver, String date);
        
        List<IMessage> RecieveFriendRegistry(int FriendGLKNumber);
        

}




