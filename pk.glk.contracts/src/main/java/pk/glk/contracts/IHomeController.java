/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.contracts;

import java.util.List;

/**
 *
 * @author Patryk
 */
public interface IHomeController {
    int RegisterUser(IUser user);
    void LogIn(String number, String password);
    void LogOut();
    void StartNewConversation(int number);
    Boolean ChangeStatus(Boolean IsOnline);
    
    void OpenFriendArchive(int FriendGLKNumber);
    
}
