package pk.glk.contracts;



/**
 *
 * @author Piotrek
 */
public interface IContactInfo {
    
    int getGLKNumber();
    String getFirstname();
    String getSurname();
    String getEMail();
    Boolean getStatus();
    String getUserIP();
    String getBirthday();
}




