/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.homecontroller.internal;

import pk.glk.contracts.IUser;
/**
 *
 * @author Patryk
 */
public class User implements IUser  {

    private final String name;
    private final String surname;
    private final String email;
    private final String password;
    
    public User(String name, String surname, String email, String password)
    {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public String getEMail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }
    
    
    
}
