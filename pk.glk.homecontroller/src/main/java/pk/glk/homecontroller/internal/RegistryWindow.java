/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.homecontroller.internal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;
import pk.glk.contracts.IContactInfo;
import pk.glk.contracts.IContactsController;
import pk.glk.contracts.IConversationManager;

import pk.glk.contracts.IHomeController;
import pk.glk.contracts.IGUIModel;
import pk.glk.contracts.IMessage;
/**
 *
 * @author Piotrek
 */
@Component
public class RegistryWindow extends javax.swing.JFrame implements PropertyChangeListener {

    /**
     * Creates new form ConversationWindow
     */
    private Map<String,JPanel> conversationPanels;
    private Map<String,JScrollPane> conversationScrollPanes;
    private BundleContext bc;
    
    @Reference
    private IHomeController HC;
    
    @Reference
    private IGUIModel GUIModel;
    
    @Reference
    private IConversationManager ConvMgr;
    
    @Reference
    private IContactsController CC;
    
    protected void bindHC(IHomeController HC)
    {
        this.HC = HC;
    }
    
    protected void bindHC(IConversationManager cm)
    {
        this.ConvMgr = cm;
    }
    
    protected void bindCC(IContactsController CC)
    {
        this.CC = CC;
    }
    
    @Activate
    public void activate(BundleContext bc)
    {
        this.bc = bc;
    }
    
    protected void bindGUIModel(IGUIModel GUIModel)
    {
        this.GUIModel = GUIModel;
        this.GUIModel.RegisterPropertyChangeListener(this);
    }
    
    public RegistryWindow() {
        initComponents();
        //this.pack();
        conversationPanels = new HashMap<>();   
        conversationScrollPanes = new HashMap<>();
        this.setVisible(false);  
        
        //System.out.println("----- Archive RegistryWindow 222 -----");
    }
    
    public void createNewTab(String username)
    {
        JPanel scrollpanel = new JPanel(new BorderLayout());
        scrollpanel.setBackground(Color.red);
        JScrollPane scroll = new JScrollPane();
        
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setAutoscrolls(true);
        //scroll.setPreferredSize(new Dimension(100,100));
        scrollpanel.add(scroll,BorderLayout.CENTER);
        
        
        JPanel newpanel = new JPanel();
        newpanel.setLayout(new BoxLayout(newpanel, BoxLayout.Y_AXIS));
        newpanel.setAutoscrolls(true);
        //newpanel.setSize(this.tpActiveConversations.getWidth(), this.tpActiveConversations.getHeight());
        
        //lista paneli, na które trzeba wrzucać nowe wiadomości
        conversationPanels.put(username, newpanel);
        conversationScrollPanes.put(username, scroll);
        
        scroll.add(newpanel);
        tpActiveConversations.addTab(this.GetUsernameFromNumber(Integer.parseInt(username)), scrollpanel);
    }
    
    
    public void createNewTab()
    {
        JPanel scrollpanel = new JPanel(new BorderLayout());
        scrollpanel.setBackground(Color.red);
        JScrollPane scroll = new JScrollPane();
        
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setAutoscrolls(true);
        //scroll.setPreferredSize(new Dimension(100,100));
        scrollpanel.add(scroll,BorderLayout.CENTER);
        
        
        JPanel newpanel = new JPanel();
        newpanel.setLayout(new BoxLayout(newpanel, BoxLayout.Y_AXIS));
        newpanel.setAutoscrolls(true);
        //newpanel.setSize(this.tpActiveConversations.getWidth(), this.tpActiveConversations.getHeight());
        
        //lista paneli, na które trzeba wrzucać nowe wiadomości
        conversationPanels.put("brak rozmow", newpanel);
        conversationScrollPanes.put("brak rozmow", scroll);
        
        scroll.add(newpanel);
        tpActiveConversations.addTab("brak rozmow", scrollpanel);
    }
    
    
    public void createNewTab(String username, String username2)
    {
        JPanel scrollpanel = new JPanel(new BorderLayout());
        scrollpanel.setBackground(Color.red);
        JScrollPane scroll = new JScrollPane();
        
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setAutoscrolls(true);
        //scroll.setPreferredSize(new Dimension(100,100));
        scrollpanel.add(scroll,BorderLayout.CENTER);
        
        
        JPanel newpanel = new JPanel();
        newpanel.setLayout(new BoxLayout(newpanel, BoxLayout.Y_AXIS));
        newpanel.setAutoscrolls(true);
        //newpanel.setSize(this.tpActiveConversations.getWidth(), this.tpActiveConversations.getHeight());
        String name = username + " --> "+username2;
        //lista paneli, na które trzeba wrzucać nowe wiadomości
        conversationPanels.put(name, newpanel);
        conversationScrollPanes.put(name, scroll);
        
        String nameToTab = this.GetUsernameFromNumber(Integer.parseInt(username)) + " --> "+this.GetUsernameFromNumber(Integer.parseInt(username2));
        
        scroll.add(newpanel);
        tpActiveConversations.addTab(nameToTab, scrollpanel);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        tpActiveConversations = new javax.swing.JTabbedPane();

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tpActiveConversations, javax.swing.GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tpActiveConversations, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify                     
    private javax.swing.JTabbedPane tpActiveConversations;
    // End of variables declaration                   

    
    
    
    
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {       
        if(evt.getPropertyName() == "OpenFriendArchive")
        {
            System.out.println("----- Archive RegistryWindow OpenFriendArchive -----");
            List<IMessage> messages = (List<IMessage>)evt.getNewValue();
            System.out.println(messages);

            
            if(messages.size() > 0){
                int MyGLKNumber = messages.get(0).GetSenderNumber();
                int FriendGLKNumber = messages.get(0).GetReceiverNumber();
                
                String friendGLKNumber = String.valueOf(FriendGLKNumber);
                String myGLKNumber = String.valueOf(MyGLKNumber);
                
                String name = myGLKNumber + " --> "+friendGLKNumber;
                
                if(!conversationPanels.containsKey(name))
                    createNewTab(myGLKNumber, friendGLKNumber);
                
                
                
                for (int i = 0; i < messages.size(); i++) {
                    MessagePanel m = new MessagePanel(this.GetUsernameFromNumber(messages.get(i).GetSenderNumber()) + ":","<html><body>" + messages.get(i).GetMessage() + "</body></html>");
                    m.setAlignmentX(JPanel.LEFT_ALIGNMENT);
                    //jScrollPane1.add(m);
                    //String friendGLKNumber = String.valueOf(FriendGLKNumber);
                    //System.out.print(conversationPanels);
                    conversationPanels.get(name).add(m);
                }
                //

                
                
                
                
                
                
                JScrollPane js = conversationScrollPanes.get(name);
                js.revalidate();
                js.repaint();
                js.setViewportView(conversationPanels.get(name));
                this.setVisible(true);

            }
            else
            {
                if(!conversationPanels.containsKey("brak rozmow"))
                    createNewTab();
                
                JScrollPane js = conversationScrollPanes.get("brak rozmow");
                js.revalidate();
                js.repaint();
                js.setViewportView(conversationPanels.get("brak rozmow"));
                
            }
            
            this.setVisible(true);
        }
        
        if(evt.getPropertyName() == "bLoggedIn")
        {         
            if((boolean)evt.getNewValue() == false)              
                this.setVisible(false);
        }      
    }
    
    private String GetUsernameFromNumber(int number)
    {
        List<IContactInfo> lista = CC.GetContactList();
        
        for(IContactInfo c : lista)
        {
            if(c.getGLKNumber() == number)
                return c.getFirstname() + " " + c.getSurname();
        }
        return String.valueOf(number);
    }
}

