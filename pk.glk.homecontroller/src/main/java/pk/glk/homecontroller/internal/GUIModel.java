/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.homecontroller.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;
import pk.glk.contracts.IGUIModel;
import pk.glk.contracts.IMessage;

/**
 *
 * @author Patryk
 */
@Service
@Component
public class GUIModel implements IGUIModel {
   
    
    private BundleContext bc;
    
    private boolean bLoggedIn;
    private PropertyChangeSupport changes;
    
    public GUIModel()
    {
         bLoggedIn = false;
         changes = new PropertyChangeSupport(this);
    }
    
    @Activate
    protected void activate(BundleContext bc)
    {
        this.bc = bc;
        System.out.println("GUIModel started");
    }

    @Override
    public void RegisterPropertyChangeListener(PropertyChangeListener pcl) {
        changes.addPropertyChangeListener(pcl);
    }
    
    @Override
    public void SetLoggedIn(boolean value)
    {     
        changes.firePropertyChange("bLoggedIn", bLoggedIn, value);
        this.bLoggedIn = value;      
    }

    @Override
    public void NewMessageReceived(IMessage msg) {
        //oldValue - senderNumber
        //newValue - message
        changes.firePropertyChange("NewMessage", null, msg);
    }

    @Override
    public void NewConversation(int number) {
        //System.out.println("---------- fire new coversation event");
        changes.firePropertyChange("StartNewConversation", null, number);
    }

    @Override
    public void FriendArchive(List<IMessage> msg) {
        changes.firePropertyChange("OpenFriendArchive", null, msg);
        //System.out.println("----- Archive FriendArchive with OpenFriendArchive: " + msg);
    }
    
}
